import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import {AngularFireDatabase} from '@angular/fire/database';
import {map} from 'rxjs/operators';
import {BehaviorSubject} from 'rxjs';
import {AngularFireFunctions} from '@angular/fire/functions';

@Component({
    selector: 'app-tables',
    templateUrl: './tables.component.html',
    styleUrls: ['./tables.component.scss'],
    animations: [routerTransition()]
})
export class TablesComponent implements OnInit {
    nombre: any;
    precio: any;
    cantidad: any;
    categoria: any;
    tienda: any;
    ednombre: any;
    edprecio: any;
    edcantidad: any;
    edcategoria: any;
    key: any;
    add  = false;
    edit  = false;
    type  = false;

    lisUser: any;

  allProducts: any;
    allStores: any;
    constructor(private db: AngularFireDatabase, private fn: AngularFireFunctions) {}

    ngOnInit() {
        const _self = this;
        setTimeout(function () {
            _self.getProductos('Tecnologia');
            _self.getStores();
        }, 1000);
     //   console.log(this.allProducts);
    }

    showForm() {
        this.add = true;
    }
    addProduct() {
        const _self = this;
        const docRef = _self.db.database.ref('productos' ).push({
            nombre: _self.nombre,
            cantidad: _self.cantidad,
            precio: _self.precio,
            categoria: _self.categoria,
            tienda: this.tienda
        }).then( async (productRegist) => {
            console.warn(productRegist);
            this.add = false;

        }).catch(function(error) {
            // An error happened.
            console.log(error);
        });
    }


     getAProduct() {
        const _self = this;
         const ref = _self.db.database.ref('productos').on('value', function(snapshot) {
             console.log('Previous Post ID: ', Object.keys(snapshot.val()));
             console.log(snapshot.val() );
             /*const member = { 'uid': Object.keys(snapshot.val()) , 'data' : snapshot.val()};
             _self.allProducts.push(member);
             */
             _self.allProducts = snapshot.val();
             console.log(_self.allProducts);
         });
    }

    getProductos(type) {
      /*  this.allProducts = this.db.list('/productos').snapshotChanges().pipe(
        map(changes =>
            changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
        )
    );*/
      /*  const query = this.db.database.ref('productos')
            .orderByChild('categoria');
        query.once('value', function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childKey = childSnapshot.key;
                var childData = childSnapshot.val();
                console.log(childData);
                // ...
            });
        });*/
   /*  const _self = this;
     _self.allProducts = [];
        const ref = this.db.database.ref('productos');
        ref.orderByChild('categoria').equalTo(type).on('value', function(snapshot) {
            console.log( snapshot.val());
            const member =   snapshot.val() ;
            _self.allProducts.push(member);

        });
       console.log(_self.allProducts);*/

        this.allProducts = this.db.list('/productos', ref =>
            ref.orderByChild('categoria').equalTo(type)).snapshotChanges().pipe(
            map(changes =>
                changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
            )
        );
    }


     removeAProduct(key) {
        const _self = this;
         const qr = _self.db.object(`productos/${key}`);
         qr.remove().then( () => {
             alert('echo');
         });
    }

    calleditAProduct(key, nombre, cantidad, precio, categoria) {
        this.edit = true;
        this.ednombre = nombre;
        this.edprecio = precio;
        this.edcantidad = cantidad;
        this.edcategoria = categoria;
       this.key = key;
    }
    editAProduct() {
        const _self = this;

         const docRef = _self.db.database.ref('productos/' +  this.key ).update({
            nombre: _self.ednombre,
            cantidad: _self.edcantidad,
            precio: _self.edprecio,
             categoria: _self.edcategoria
        }).then( async (productRegist) => {
            console.warn(productRegist);
             this.edit = false;

        }).catch(function(error) {
            // An error happened.
            console.log(error);
        });
    }

    getStores() {
        this.allStores = this.db.list('/tiendas').snapshotChanges().pipe(
            map(changes =>
                changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
            )
        );
        console.log(this.allStores);
    }
}
