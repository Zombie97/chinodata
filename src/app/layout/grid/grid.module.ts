import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GridRoutingModule } from './grid-routing.module';
import { GridComponent } from './grid.component';
import { PageHeaderModule } from './../../shared';
import {SweetAlert2Module} from '@sweetalert2/ngx-sweetalert2';

@NgModule({
    imports: [CommonModule, GridRoutingModule, PageHeaderModule, SweetAlert2Module],
    declarations: [GridComponent]
})
export class GridModule {}
