import {Component, OnInit, ViewChild} from '@angular/core';
import { routerTransition } from '../../router.animations';
import {map} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularFireDatabase} from '@angular/fire/database';
import swal from "sweetalert2";
import {SwalComponent} from '@sweetalert2/ngx-sweetalert2';

@Component({
    selector: 'app-grid',
    templateUrl: './grid.component.html',
    styleUrls: ['./grid.component.scss'],
    animations: [routerTransition()]
})
export class GridComponent implements OnInit {
    allProducts: any;
    storeInfo: any;
    allProductsArr = [];
    @ViewChild('deleteSwal') private deleteSwal: SwalComponent;
    constructor(private db: AngularFireDatabase, private route: ActivatedRoute,  public router: Router) {}

    ngOnInit() {
        setTimeout(() => {
            this.getInfo();
            this.getStore();
        }, 500);

    }
    fill(data)  {
   /*     this.allProductsArr.push(data);

        console.log('ssssssssssssssssssss');
        console.log(this.allProductsArr);


*/
   console.log(data);
        if (data === 0 ) {
            swal.fire({
                type: 'warning',
                title: 'Vacio',
                text: 'No existen productos en la tienda',
                confirmButtonColor: '#049F0C',
                confirmButtonText: 'Regresar '
            }).then((result) => {
                if (result.value) {
                    this.router.navigate(['forms']);
                } else {
                    swal.fire({
                        type: 'error',
                        title: '¡Error!',
                        text: 'La operacion se cancelo',
                        timer: 2000,
                        showConfirmButton: false
                    });
                }
               }); }
    }
    getInfo() {
        this.route.params.subscribe(params => {
            this.allProducts = this.db.list('/productos', ref =>
                ref.orderByChild('tienda').equalTo(params['id'])).snapshotChanges().pipe(
                map(changes => changes.map(  c => ( {key: c.payload.key, ...c.payload.val()}  ), (this.fill(changes.length)) )

                )
            );
            console.log(this.allProducts );

        });

    }

    getStore() {
        this.route.params.subscribe(params => {
            this.storeInfo = this.db.list('/tiendas', ref =>
                ref.orderByChild('key').equalTo(params['id'])).snapshotChanges().pipe(
                map(changes =>
                    changes.map(c => ({key: c.payload.key, ...c.payload.val()}))
                )
            );
        });
        console.log(this.storeInfo);
    }
    goBack() {
        this.router.navigate(['forms']);
    }
}
