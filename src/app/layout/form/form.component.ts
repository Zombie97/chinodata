import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import {AngularFireDatabase} from '@angular/fire/database';
import {map} from 'rxjs/operators';
import swal from 'sweetalert2';
import {Router} from '@angular/router';
@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss'],
    animations: [routerTransition()]
})
export class FormComponent implements OnInit {
    nombre: any;
    Domicilio: any;
    key: any;
    add  = false;
    edit  = false;


    allStores: any;
    constructor(private db: AngularFireDatabase,
                public router: Router) {}

    ngOnInit() {
        const _self = this;
        setTimeout(function () {
            _self.getProductos();
        }, 1000);
        //   console.log(this.allProducts);
    }

    showForm() {
        this.add = true;
    }
    addProduct() {
        const _self = this;
        const docRef = _self.db.database.ref('tiendas' ).push({
            nombre: _self.nombre,
            Domicilio: _self.Domicilio
        }).then( async (productRegist) => {
            console.warn(productRegist);
            this.add = false;

        }).catch(function(error) {
            // An error happened.
            console.log(error);
        });
    }




    getProductos() {
        this.allStores = this.db.list('/tiendas').snapshotChanges().pipe(
            map(changes =>
                changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
            )
        );
            console.log(this.allStores);
    }


    removeAProduct(key) {
        swal.fire({
            type: 'warning',
            title: '¿Estas seguro de eliminarlo?',
            text: 'No se podra recuperar esta tienda',
            showCancelButton: true,
            confirmButtonColor: '#049F0C',
            cancelButtonColor:'#ff0000',
            confirmButtonText: 'Si borrar ',
            cancelButtonText: 'No, Cancelar'
        }).then((result) => {
            if (result.value) {
                const _self = this;
                const qr = _self.db.object(`tiendas/${key}`);
                qr.remove().then( () => {
                    swal.fire({
                        type: 'success',
                        title: 'Eliminado',
                        text: 'La tienda fue eliminada.',
                    });
                });
            } else {
                swal.fire({
                    type: 'error',
                    title: '¡Error!',
                    text: 'La operacion se cancelo',
                    timer: 2000,
                    showConfirmButton: false
                });
            }

        }, (dismiss) => {
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal.fire({
                    type:'info',
                    title: 'Cancelled',
                    text: 'Your Staff file is safe :)'
                });
            }
        });

    }

    calleditAProduct(key, nombre, Domicilio) {
        this.add = true;
        this.edit = true;
        this.nombre = nombre;
        this.Domicilio = Domicilio;
        this.key = key;
    }
    editAProduct() {
        const _self = this;

        const docRef = _self.db.database.ref('tiendas/' +  this.key ).update({
            nombre: _self.nombre,
            Domicilio: _self.Domicilio,
        }).then( async (productRegist) => {
            console.warn(productRegist);
            this.add = false;
            this.edit = false;

        }).catch(function(error) {
            // An error happened.
            console.log(error);
        });
    }
    detailStrore(id) {
        console.log(id);
        this.router.navigate(['/grid', id]);
    }
}
