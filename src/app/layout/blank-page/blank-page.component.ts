import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {AngularFireFunctions} from '@angular/fire/functions';
import {AngularFireAuth} from '@angular/fire/auth';
import swal from "sweetalert2";
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
    selector: 'app-blank-page',
    templateUrl: './blank-page.component.html',
    styleUrls: ['./blank-page.component.scss']
})
export class BlankPageComponent implements OnInit {
    UserList: any;
    newName: any;
    edit: any;
    disabled: any;
    constructor(private db: AngularFireDatabase, private fn: AngularFireFunctions, private fire: AngularFireAuth, private http: HttpClient) {}

  async  ngOnInit() {
      await  this.getList();
    }
    getList() {
        const data = '';
        this.fn.functions.httpsCallable('listUser')(data).then( (info) => {
            console.warn(info.data.users);
            this.UserList = info.data.users;
        });
    }
    show(uid) {
        this.edit = true;
        localStorage.setItem('uid', uid);
    }
    async UpdateName() {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'Authorization': 'secret-key'
            })
        };
        const postData = {
            uid: localStorage.getItem('uid'),
            displayName: this.newName};

        this.fn.functions.httpsCallable('update')(postData).then( (info) => {
            console.warn(info);
            this.presentAlert( 'success', 'Actualizado', info );
            this.getList();
            this.edit = false;
        }).catch( err => {
            console.log('Error occured');
            console.log(err);
            this.presentAlert('error', 'No se pudo actualizar', err);
            this.edit = false;
        });
  /*      this.http.post('https://us-central1-sisabiertos-5e7e0.cloudfunctions.net/update', postData, httpOptions)
            .subscribe(
                res => {
                    console.log(res);  this.presentAlert( 'success', 'Actualizado', res );
                    this.edit = false;
                },
                err => {
                    console.log('Error occured');
                    console.log(err);
                    this.presentAlert('error', 'No se pudo actualizar', err);
                    this.edit = false;
                });*/
    }
    async UpdateStatus(uid, disabled) {
        const postData = {
            uid: uid,
            disabled: disabled
        };

        this.fn.functions.httpsCallable('updateStatus')(postData).then( (info) => {
            console.warn(info);
            this.presentAlert( 'success', 'Actualizado', info );
            this.getList();
            this.edit = false;
        }).catch( err => {
            console.log('Error occured');
            console.log(err);
            this.presentAlert('error', 'No se pudo actualizar', err);
            this.edit = false;
        });
    }
    async DeleteUser(uid) {
        const postData = {
            uid: uid
              };

        this.fn.functions.httpsCallable('deleteUser')(postData).then( (info) => {
            console.warn(info);
            this.presentAlert( 'success', 'Eliminado', info );
            this.getList();
            this.edit = false;
        }).catch( err => {
            console.log('Error occured');
            console.log(err);
            this.presentAlert('error', 'No se pudo eliminar', err);
            this.edit = false;
        });

    }

    presentAlert(type, title, txt) {
        swal.fire({
            type: type,
            title: title,
            text: txt,
            confirmButtonColor: '#049F0C',
            confirmButtonText: 'Regresar '
        });
    }
}
