import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFireDatabase} from '@angular/fire/database';
import {Router} from '@angular/router';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    displayName: any;
    email: any;
    pass: any;
    constructor(private fire: AngularFireAuth, private db: AngularFireDatabase, public router: Router) {}

    ngOnInit() {}
    registerUser() { //crear usuarios y mandar correo de verificacion
        const _self = this;
        if (this.displayName == null || this.pass == null  || this.email == null ) {
            alert('Hey!, Llena todos los campos');
        } else {
                this.fire.auth.createUserWithEmailAndPassword(this.email, this.pass)
                    .then(data => {
                        const user = this.fire.auth.currentUser;
                        user.sendEmailVerification();
                        user.updateProfile({
                            displayName: this.displayName
                        }).then(function() {
                            // Update successful.
                            const docRef = _self.db.list('usuarios');
                            docRef.push({
                                uid: user.uid,
                                displayName: _self.displayName,
                                email: _self.email,
                                pass: _self.pass
                            }).then( async (UserRegist) => {
                                console.warn(UserRegist);
                                _self.registerUserset();
                            });

                        }).catch(function(error) {
                            // An error happened.
                            console.log(error);
                        });
                    })
                    .catch(error => {
                        console.log('got an error ', error);
                        alert(error.message);
                    });
                console.log('Would register user with ', this.email);
            }
        }
    registerUserset() {
        const _self = this;
        if (this.displayName == null || this.pass == null  || this.email == null ) {
            alert('Hey!, Llena todos los campos');
        } else {
                    const user = this.fire.auth.currentUser;
                        const docRef = _self.db.database.ref('usuarios/' + user.uid).set({
                            uid: user.uid,
                            displayName: _self.displayName,
                            email: _self.email,
                            pass: _self.pass
                        }).then( async (UserRegist) => {
                            console.warn(UserRegist);
                            localStorage.setItem('user', JSON.stringify(user));
                            localStorage.setItem('isLoggedin', 'true');
                            _self.router.navigateByUrl('/dashboard');

                    }).catch(function(error) {
                        // An error happened.
                        console.log(error);
                    });

        }
    }

}
