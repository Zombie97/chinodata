import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFireDatabase} from '@angular/fire/database';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    email: any;
    pass: any;
    newpass: any;
    showPP = true;
    user: any;
    constructor(public router: Router, private fire: AngularFireAuth, private db: AngularFireDatabase) {
        this.user = this.fire.auth.currentUser;
    }

    ngOnInit() {
    }

    login() {
        this.fire.auth.signInWithEmailAndPassword(this.email , this.pass).then( data => {
            localStorage.setItem('user', JSON.stringify(data.user));
            console.log(data); console.log('got some data');
            localStorage.setItem('isLoggedin', 'true');
            this.router.navigateByUrl('/dashboard');
        }).catch( (err) => {
                console.warn( err);
            });
    }
    showp() {
        this.showPP = false;
    }
    updatePass() {
        const _self = this;
        if (this.newpass == null  ) {
            alert('Hey!, Llena todos los campos');
        } else {
           /* _self.user.updateProfile({
                displayName: _self.newpass
            }).then( () => {
                console.log('rady');
            });*/
            const docRef = _self.db.database.ref('usuarios/' + _self.user.uid).update({
                pass: _self.newpass,
            }).then( async (data) => {
                console.warn(data);
                _self.showPP = true;

            }).catch(function(error) {
                // An error happened.
                console.log(error);
            });

        }
    }
}
