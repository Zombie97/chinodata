import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin'
// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
admin.initializeApp(functions.config().firebase);

export const updateUser = functions.database.ref('usuario').onUpdate( (snap,context) => {
    console.log(snap);
});


export const update = functions.https.onCall( async (data, context) => {
    await admin.auth().updateUser( data.uid, {
        displayName: data.displayName,
    });
    return 'update user';
});
export const updateStatus = functions.https.onCall( async (data, context) => {
    await admin.auth().updateUser( data.uid, {
        disabled: data.disabled,
    });
    return 'update user';
});
export const deleteUser = functions.https.onCall( async (data, context) => {
    await admin.auth().deleteUser( data.uid);
    return 'update user';
});
export const listUser = functions.https.onCall( async (data, context) => {

    return admin.auth().listUsers();
});

